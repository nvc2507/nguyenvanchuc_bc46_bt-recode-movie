import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    chairBookings: [],
    chairBookeds: [],
}

const BTDatVeSlice = createSlice({
    name: 'BTDatVe',
    initialState,
    reducers: {
        setChairBookings: (state, action) => {
            const index = state.chairBookings.findIndex((e) => e.soGhe === action.payload.soGhe)
            //Kiểm tra xem ghế có tồn tại trong mảng chairbookings hay không
            if(index !== -1) {
                state.chairBookings.splice(index, 1)
            }else{
                // nếu chưa tồn tại thì push vào mảng chairbookings
                state.chairBookings.push(action.payload)
            }
        },
        setChairBookeds: (state, payload) => {
            state.chairBookeds = [...state.chairBookeds, ...state.chairBookings]
            state.chairBookings =[]
        }
    }
})

export const {actions: btDatVeAction, reducer: btDatVeReducer} = BTDatVeSlice