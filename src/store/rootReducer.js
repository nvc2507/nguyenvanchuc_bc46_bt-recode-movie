import { combineReducers } from "@reduxjs/toolkit";
import { btDatVeReducer } from "./slice";


export const rootReducer = combineReducers({
    btDatVe: btDatVeReducer,
})