import React from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { btDatVeAction } from '../store/slice'

const Result = () => {
  const {chairBookings} = useSelector((state) => state.btDatVe)
  const dispatch = useDispatch()
  return (
    <div className=' mt-3 '>
      <h1 className='text-center  bg-secondary  text-white  font-weight-bold'>Danh sách ghế bạn chọn</h1>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark mt-3 Chair booked poiter">Ghế đã đặt</button>
        <button className="btn btn-outline-dark mt-3 Chair booking poiter">Ghế đang chọn</button>
        <button className="btn btn-outline-dark mt-3 poiter">Ghế hiện đang trống</button>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>Số ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {
            chairBookings.map((ghe)=> (
              <tr key={ghe.soGhe}>
                <td>{ghe.soGhe}</td>
                <td>{ghe.gia}</td>
                <td>
                  <button className="btn btn-danger"
                  onClick={()=> dispatch(btDatVeAction.setChairBookings(ghe))}>Hủy</button>
                </td>
              </tr>
            ))
          }
          <tr>
            <td>Tổng tiền </td>
            <td> {chairBookings.reduce((total ,ghe) => (total += ghe.gia), 0)}</td>
          </tr>
        </tbody>
      </table>
      <button className="btn btn-success"
      onClick={()=> dispatch(btDatVeAction.setChairBookeds())}>Thanh toán</button>
    </div>
  )
}

export default Result