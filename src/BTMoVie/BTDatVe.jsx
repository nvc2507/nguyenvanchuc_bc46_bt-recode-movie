import React from "react";
import data from "./data.json";
import ChairList from "./ChairList";
import Result from "./Result";


const BTDatVe = () => {
  return (
    <div classname=" mt-5">
      <nav className="navbar navbar-expand-lg navbar-light bg-info fixed-top">
        <a className="navbar-brand  font-weight-bold " href="">
          My Cinema
        </a>
       
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item active">
              <a className="nav-link " href="#">
                Home <span className="sr-only">(current)</span>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                Book Ticket
              </a>
            </li>
           
            <li className="nav-item mr-5">
              <a className="nav-link" href="#">
                About
              </a>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              className="btn btn-warning my-2 my-sm-0"
              type="submit"
            >
              Search
            </button>
          </form>
        </div>
      </nav>

      <div className="container mt-5">
        <div className="row">
          <div className="col-8">
            <div className="text-center p-3 display-4 bg-dark text-white mt-3 font-weight-bold">SCREEN</div>
            <ChairList data={data}/>
          </div>
          <div className="col-4">
            <Result />
          </div>
        </div>
      </div>

      
    </div>

  );
};

export default BTDatVe;
