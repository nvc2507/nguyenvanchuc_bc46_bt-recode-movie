import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import cn from 'classnames'
import { btDatVeAction } from '../store/slice'
import './style.scss'

const Chair = ({ghe}) => {
  const dispatch = useDispatch()

  const {chairBookings, chairBookeds} = useSelector(state => state.btDatVe)
  return (
    <button className={cn("btn btn-outline-dark Chair",
    {
      booking: chairBookings.find((e)=> e.soGhe === ghe.soGhe),
      booked: chairBookeds.find((e)=> e.soGhe === ghe.soGhe)
    })}
    style={{width: '50px'}}
    onClick={() => {
      dispatch(btDatVeAction.setChairBookings(ghe));
    }}>
      {ghe.soGhe}
    </button>
  )
}

export default Chair