import React from 'react'
import Chair from './Chair'

const ChairList = ({data}) => {
  console.log("data",data);
  return (
    <div>
      {
        data.map((hangGhe) => {
          return (
            <div 
            key={hangGhe.hang}
              className="d-flex mt-3"
              style={{gap: "10px"}}>
              <div 
                className="text-center"
                style={{width: "40px"}}>
                {hangGhe.hang}
              </div>
              <div 
                className="d-flex"
                style={{gap: "10px"}}>
                {hangGhe.danhSachGhe.map((ghe) => {
                  return <Chair ghe={ghe}/>
                })}
              </div>
            </div>
          )
        })
      }
    </div>
  )
}

export default ChairList